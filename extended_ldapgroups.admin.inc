<?php

/**
 * @file
 * Module admin page callbacks.
 */

require_once(drupal_get_path('module', 'ldapgroups') .'/includes/LDAPInterface.inc');
require_once(drupal_get_path('module', 'ldapgroups') .'/ldapgroups.inc');
  
/**
 * Menu callback function for importing LDAP groups into Drupal as roles
 * 
 * @return
 *   FAPI array representing confirmation form
 */
function extended_ldapgroups_admin_groups_import() {
  $form = array();
  $form['confirm'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import LDAP Groups'),
    '#weight' => 0,
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
    '#description' => t('This will import all groups from LDAP into Drupal as roles.  Groups that currently '.
      'exist in Drupal will not be affected.  Please back up your datbase before peforming this function.'),
  );
  $form['confirm']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import LDAP groups'),    
  );
  return $form;
}

/**
 * Submit handler for the LDAP groups import form
 * @param 
 *   $form
 * @param 
 *   $form_state
 * @see extended_ldapgroups_admin_groups_import()
 * @todo support for Batch API
 */
function extended_ldapgroups_admin_groups_import_submit(&$form, &$form_state) {
  // Get all the groups from LDAP
  $groups = extended_ldapgroups_ldap_group_list();
  // Loop through the LDAP groups and create each one as a role
  if(!empty($groups)) {
    foreach($groups as $groupname) {
      // Create the role in Drupal
      _ldapgroups_create_role($groupname);
      
      // Double-check to make sure the role was created successfully
      $result = db_query("SELECT * FROM {role} WHERE name = '%s'", $groupname);
      if (!($row = db_fetch_object($result))) {
        watchdog('extended_ldapgroups', 'Failed to import %groupname as a Drupal role.', array('%groupname' => $groupname), WATCHDOG_ERROR);
        drupal_set_message(t('Failed to import %groupname as a Drupal role.', array('%groupname' => $groupname)), 'error');
      }
      else {
        watchdog('extended_ldapgroups', 'Successfully imported %groupname as a Drupal role.', array('%groupname' => $groupname), WATCHDOG_INFO);
        drupal_set_message(t('Successfully imported %groupname as a Drupal role.', array('%groupname' => $groupname)));
      }
    } 
  }
  else {
    watchdog('extended_ldapgroups', 'Failed to retrieve any groups from LDAP.', null, WATCHDOG_ERROR);
    drupal_set_message(t('Failed to retrieve any groups from LDAP'), 'error');
  }
}

/**
 * Form builder function to create an LDAP integration admin form.
 * 
 * @return
 *   FAPI array representing the admin form
 */
function extended_ldapgroups_admin_settings_form() {
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Integration Options'),
    '#collapsed' => false,
    '#collapsible' => true,
    '#weight' => -10,
  );
  $form['options']['extended_ldapgroups_ldap_group_remove_membership'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('ldapgroups_block_user', 0),
    '#title' => t('Remove LDAP group membership for blocked users'),
    '#description' => t('When a user is blocked in Drupal, this option will remove all group memberships for the user in LDAP.  When the user is unblocked, group membership will be restored based on the user account roles.'),
  );
  $form['options']['extended_ldapgroups_ldap_remove_user'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('ldapgroups_delete_user', 0),
    '#title' => t('Delete users in LDAP when deleted in Drupal'),
    '#description' => t('When a user account is deleted in Drupal, this option will remove the user account in LDAP.'),
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Options'),
  );
  return $form;
}

/**
 * Submit handler for the ldapgroups admin settings form
 * 
 * @param mixed $form
 * @param mixed $form_state
 * @see extended_ldapgroups_admin_settings_form()
 */
function extended_ldapgroups_admin_settings_form_submit($form, $form_state) {
  variable_set('ldapgroups_block_user', $form_state['values']['extended_ldapgroups_ldap_group_remove_membership']);
  variable_set('ldapgroups_delete_user', $form_state['values']['extended_ldapgroups_ldap_remove_user']);
}